import React from 'react'

class AppointmentDetail extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            appointments: [],
            inputVin: "",
        }
        this.handleVinChange = this.handleVinChange.bind(this);
    }
    
    async componentDidMount() {
        const url = 'http://localhost:8080/api/appointments/';
        
        const response = await fetch(url);
        
        if (response.ok) {
            const newAppointment = await response.json();
            this.setState({ appointments: newAppointment.appointments })
        }
    }

    handleVinChange(event) {
        const value = event.target.value;
        this.setState({inputVin: value})
    }   
        
    render () {
        return (
            <>
            <h1>Appointment History</h1> 
            <input onChange={this.handleVinChange} required id="inputVin" name="inputVin" 
                value={this.state.inputVin} className="form-control mr-sm-2" maxLength={17}
                type="search" placeholder="Search by Vin" aria-label="Search"/>
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>Vin</th>
                  <th>Customer Name</th>
                  <th>Date/Time</th>
                  <th>Technician</th>
                  <th>Reason</th>
                  <th>Finished?</th>
                  <th>Vip?</th>
                </tr>
              </thead>
              <tbody>
                {this.state.appointments.filter(appoint => 
                    appoint.vin === this.state.inputVin)
                    .map(appointment => {
                        return (
                            <tr key={appointment.id}>
                            <td>{ appointment.vin }</td>
                            <td>{ appointment.customer_name }</td>
                            <td>{ appointment.date_time }</td>
                            <td>{ appointment.technician.name }</td>
                            <td>{ appointment.reason }</td>      
                            <td>{ String(appointment.is_finished) }</td>
                            <td>{ String(appointment.is_vip) }</td>
                        </tr>
                        );
                })}
                
              </tbody>
            </table>
            </>
        )        
    }
}

export default AppointmentDetail
