import React from 'react'

class TechnicianForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            employee_number:"",

        }
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleEmployeeNumChange = this.handleEmployeeNumChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this)

    }

        async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        console.log(data)

    const url = 'http://localhost:8080/api/technicians/';
    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        
        }
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
        const data2 = await response.json();
        console.log(data2)
        const cleared = {
            name: '',
            employee_number:'',
          };
          this.setState(cleared);
        
    }
}
    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value });
    }
    handleEmployeeNumChange(event) {
        const value = event.target.value;
        this.setState({ employee_number: value });
    }
    
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add Technician</h1>
                        <form onSubmit={this.handleSubmit} id="create-technician-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleEmployeeNumChange} value={this.state.employee_number} placeholder="Employee Number" required type="number" name="employee_number" id="employee_number" className="form-control" />
                                <label htmlFor="employee_number">Employee Number</label>
                            </div>
                            <button className="btn btn-primary">Add Technician</button>
                        </form>
                    </div>
                </div>
            </div>  
        )
    }
}
export default TechnicianForm