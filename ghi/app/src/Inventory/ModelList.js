import React from 'react'

class ModelList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {models: []} 
    }

    async componentDidMount() {
      const response = await fetch('http://localhost:8100/api/models/');

      if (response.ok) {
        const data = await response.json();
        this.setState({ models: data.models })
      }
    } 

      render () {
        return (
            <>
            <h1>Models List</h1>
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>Manufacturer Name</th>
                  <th>Picture URL</th>
                </tr>
              </thead>
              <tbody>
                {this.state.models.map(model => {
                  return (
                  
                    <tr key={model.id}>
                      <td>{ model.manufacturer.name }</td>
                      <td><img src={ model.picture_url } className="img-thumbnail" /></td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            </>
        )        
    }
}

export default ModelList
                   
