import React from 'react'


class SalesList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {sales_records: []}
    }

    
    
    async componentDidMount() {
        const response = await fetch('http://localhost:8090/api/salesrecord/')
        if (response.ok) {
          const data = await response.json()
          this.setState({ sales_records: data.sales_records })
        }
      }
      
    render () {
        return (
            <>
            <h1>View Sales</h1>
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>Sales Person</th>
                  <th>Employee Number</th>
                  <th>Customer</th>
                  <th>VIN</th>
                  <th>Sale Price</th>
                </tr>
              </thead>
              <tbody>
                {this.state.sales_records.map(salesrecords => {
                  return (
                    <tr key={salesrecords.id}>
                      <td>{ salesrecords.salesperson.name }</td>
                      <td>{ salesrecords.salesperson.employee_number }</td>
                      <td>{ salesrecords.customer.name }</td>
                      <td>{ salesrecords.automobile.vin }</td>
                      <td>{ salesrecords.sale_price }</td>                 
                    </tr>
                  );
                })}
              </tbody>
            </table>
            </>
        )        
    }
}

export default SalesList
