import React from 'react';

class SalesRecordForm extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            automobile: '',
            automobiles: [],
            salesperson: '',
            salespersons: [],
            customer: '',
            customers: [],
            sale_price: '',

        }
        this.handleAutomobileChange = this.handleAutomobileChange.bind(this)
        this.handleSalespersonChange = this.handleSalespersonChange.bind(this)
        this.handleCustomerChange = this.handleCustomerChange.bind(this)
        this.handleSalePriceChange = this.handleSalePriceChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }


    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.automobiles
        delete data.salespersons
        delete data.customers       

        const salesRecordUrl = 'http://localhost:8090/api/salesrecord/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(salesRecordUrl, fetchConfig);
        if (response.ok) {
            const newSalesRecord = await response.json();
            console.log(newSalesRecord);

            const cleared = {
                automobile: '',
                salesperson: '',
                customer: '',
                sale_price: '',
            };
            this.setState(cleared);
        }
    }

    handleAutomobileChange(event) {
        const value = event.target.value;
        this.setState({automobile: value})
    }

    handleSalespersonChange(event) {
        const value = event.target.value;
        this.setState({salesperson: value})
    }

    handleCustomerChange(event) {
        const value = event.target.value;
        this.setState({customer: value})
    }

    handleSalePriceChange(event) {
        const value = event.target.value;
        this.setState({sale_price: value})
    }

    async componentDidMount(){
        const automobileUrl = 'http://localhost:8100/api/automobiles/'
        const salespersonUrl = 'http://localhost:8090/api/salesperson/'
        const customerUrl = 'http://localhost:8090/api/customer/'


        const response = await fetch(automobileUrl)
        const response2 = await fetch(salespersonUrl)
        const response3 = await fetch(customerUrl)
        
        if(response.ok && response2.ok && response3.ok) {
            const data_automobile = await response.json();
            const data_salesperson = await response2.json();
            const data_customer = await response3.json();

            this.setState({automobiles: data_automobile.autos})
            this.setState({salespersons: data_salesperson.salespersons})
            this.setState({customers: data_customer.customers})
            console.log("state", this.state)
        }
    }
        render () {
            return (
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1>Add a sales record</h1>
                            <form onSubmit={this.handleSubmit} id="create-sale-record-form">                        
                            <div className="mb-3">
                                <select onChange={this.handleAutomobileChange} value={this.state.automobile} required name="automobile" id="automobile" className="form-select">
                                <option value="">Choose an automobile</option>
                                {this.state.automobiles.map(auto => {
                                    return (
                                        <option key={auto.id} value={auto.vin}>
                                        {auto.model.manufacturer.name} / {auto.model.name} / {auto.vin}
                                        </option>
                                    )
                                })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleSalespersonChange} value={this.state.salesperson} required name="salesperson" id="salesperson" className="form-select">
                                <option value="">Choose a salesperson</option>
                                {this.state.salespersons.map(person => {
                                    return (
                                        <option key={person.id} value={person.id}>
                                        {person.name}
                                        </option>
                                    )
                                })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleCustomerChange} value={this.state.customer} required name="customer" id="customer" className="form-select">
                                <option value="">Choose a customer</option>
                                {this.state.customers.map(cust => {
                                    return (
                                        <option key={cust.id} value={cust.id}>
                                        {cust.name}
                                        </option>
                                    )
                                })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleSalePriceChange} value={this.state.sale_price} placeholder="Sale Price" required type="text" name="sale_price" id="sale_price" className="form-control"/>
                                <label htmlFor="sale_price">Sale Price</label>
                            </div>                        
                            <button className="btn btn-primary">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            )
        }
    }
    export default SalesRecordForm;
