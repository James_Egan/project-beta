import React from 'react'

class SalesPersonHistory extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            sales_records: [],
            salesperson: [],
            salespersonid: "",
            selectedsales: []
        }
        this.handleSalespersonChange = this.handleSalespersonChange.bind(this)
        this.handleSalesRecordsChange = this.handleSalesRecordsChange.bind(this)
    }

    async handleSalespersonChange(event) {
        const value = event.target.value;
        await this.setState({salespersonid: value})
        let emptyrecords = []
         
        
        for (let sale in this.state.sales_records) {
            
            if (this.state.sales_records[sale].salesperson.id == this.state.salespersonid) {
                emptyrecords.push(this.state.sales_records[sale])
            }

            
        }
        this.setState({selectedsales: emptyrecords})
        
        }
    async handleSalesRecordsChange(event) {
        const value = event.target.value;
        this.setState({sales_records: value})

        }
    
    


    async componentDidMount() {
        const response = await fetch('http://localhost:8090/api/salesperson/')
        const response2 = await fetch('http://localhost:8090/api/salesrecord/')
        if (response.ok && response2.ok) {
          const data = await response.json()
          const data2 = await response2.json()
          
          this.setState({ salesperson: data.salespersons })
          this.setState({ sales_records: data2.sales_records })
        }
      }  

    render () {
        return (
            <>
            <h1>Sales person history</h1>
            <div className="form-floating mb-3">
            <select onChange={this.handleSalespersonChange} required name="salesperson" id="salesperson" className="form-select">
                <option value="">Choose a Sales Person</option>
                {this.state.salesperson.map(salespersons => {
                return (
                    <option key={salespersons.id} value={salespersons.id}>
                    {salespersons.name}
                    </option>
                );
                })} 
            </select>
            </div>
            
            <table className="table table-striped table-hover table-bordered">
            <caption>List of sale records</caption>
              <thead>
                <tr>
                  <th>Sales person</th>
                  <th>Customer</th>
                  <th>Vin</th>
                  <th>Sale price</th>
                  
                </tr>
              </thead>
              <tbody>
                {this.state.selectedsales.map(salesrecords => {
                  return (
                    <tr key={salesrecords.id}>
                      <td>{ salesrecords.salesperson.name }</td>
                      <td>{ salesrecords.customer.name }</td>
                      <td>{ salesrecords.automobile.vin }</td>
                      <td>{ salesrecords.sale_price }</td>
                                       
                    </tr>
                  );
                })
            }
            
              </tbody>
            </table>
            </>
        )
                    
    }
}
export default SalesPersonHistory; 