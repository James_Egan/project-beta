import "./styles.css";

function MainPage() {
  return (
    <>
    <div className="px-4 py-5 my-10 text-center">
      <h1 className="display-2 fw-bold">CarCar</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-1">
          The premiere solution for automobile dealership
          management!
        </p>
      </div>
    </div>
    <picture>
    <img
        src="https://media.istockphoto.com/photos/car-engine-detail-picture-id856707194?k=20&m=856707194&s=612x612&w=0&h=R1lKMZORoCqWsysjGY0KfiFvtLcyhxSL9abRSheR4jw="
        alt="car"
      />
    </picture>
    </>

  );
}

export default MainPage;
