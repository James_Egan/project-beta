import { NavLink } from 'react-router-dom';

function Nav() {
 
   return (
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container-fluid d-flex justify-content-start">
         <NavLink className="navbar-brand" to="/"><h1>CarCar</h1></NavLink>

          {/* Navbar parent */}
          <div className="collapse navbar-collapse" id="navbarNavDarkDropdown">
            <ul className="navbar-nav">

              {/* dropdown1 starts here */}
              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Inventory
                </a>
                {/* {theList} */}
                <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                  <li className="nav-item">
                     <NavLink className="navbar-brand"  to="/automobiles">Automobile List</NavLink>
                 </li>
                 <li className="nav-item">
                     <NavLink className="navbar-brand"  to="/manufacturers">Manufacturer List</NavLink>
                 </li>
                 <li className="nav-item">
                     <NavLink className="navbar-brand"  to="/models">Model List</NavLink>
                 </li>
                 <li className="nav-item">
                     <NavLink className="navbar-brand"  to="/automobiles/new">Add Automobile</NavLink>
                 </li>
                 <li className="nav-item">
                     <NavLink className="navbar-brand"  to="/models/new">Add Model</NavLink>
                 </li>
                 <li className="nav-item">
                     <NavLink className="navbar-brand"  to="/manufacturers/new">Add Manufacturer</NavLink>
                 </li>
                </ul>
              </li>

              {/* dropdown 2 */}
              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Services
                </a>
                <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                 <li className="nav-item">
                     <NavLink className="navbar-brand"  to="/appointments">Appointment List </NavLink>
                 </li>
                 <li className="nav-item">
                     <NavLink className="navbar-brand"  to="/appointments/history">Service Appointment History </NavLink>
                 </li>
                 <li className="nav-item">
                     <NavLink className="navbar-brand"  to="/technician/list">Technician List </NavLink>
                 </li>
                 <li className="nav-item">
                     <NavLink className="navbar-brand"  to="/technician/new">Add a Technician </NavLink>
                 </li>
                 <li className="nav-item">
                     <NavLink className="navbar-brand"  to="/appointments/new">Add Appointment </NavLink>
                 </li>
                </ul>
              </li>

              {/* dropdown 3 */}
              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Sales
                </a>
                <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                  {/* <li className="nav-item">
                     <NavLink className="navbar-brand"  to="/sales">Sales List</NavLink>
                 </li> */}
                 <li className="nav-item">
                     <NavLink className="navbar-brand"  to="/salesrecord/list">Sales List</NavLink>
                 </li>
                 <li className="nav-item">
                     <NavLink className="navbar-brand"  to="/salesrecord/history">Sales Person History</NavLink>
                 </li>
                 <li className="nav-item">
                     <NavLink className="navbar-brand"  to="/salesrecord">Add Sale</NavLink>
                 </li>
                 <li className="nav-item">
                     <NavLink className="navbar-brand"  to="/salesperson">Add Salesperson</NavLink>
                 </li>
                 <li className="nav-item">
                     <NavLink className="navbar-brand"  to="/customer">Add Customer</NavLink>
                 </li>
                 
                </ul>
              </li>


            </ul>
          </div>
        </div>
      </nav>

  )
}

export default Nav;
          
          

