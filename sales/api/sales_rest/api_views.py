from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json
from django.http import JsonResponse
from sales_rest.models import SalesPerson, SalesRecord, Customer, AutomobileVO



# Create your views here.
class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "vin",
        "import_href"
    ]

class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "id",
        "name",
        "employee_number"
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "name",
        "address",
        "phone_number",
    ]

class SalesRecordEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        "id",
        "automobile",
        "salesperson",
        "customer",
        "sale_price",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalesPersonEncoder(),
        "customer": CustomerEncoder()

    }

@require_http_methods(["GET", "POST"])
def api_salesperson(request):
    if request.method == "GET":
        salespersons = SalesPerson.objects.all()
        return JsonResponse(
            {"salespersons": salespersons},
            encoder=SalesPersonEncoder
    )
    else:
        try:
            content = json.loads(request.body)
            salesperson = SalesPerson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Invalid Salesperson"}
            )
            response.status_code = 400
            return response





@require_http_methods(["GET", "POST"])
def api_customer(request):
    if request.method =="GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"message": "Invalid Customer"}
            )
            response.status_code = 400
            return response




@require_http_methods(["GET", "POST"])
def api_salesrecord(request):
    if request.method =="GET":
        salespersonid = request.GET.get("salespersonid")
        if not salespersonid:
            salesrecords = SalesRecord.objects.all()
            return JsonResponse(
                {"sales_records": salesrecords},
                encoder=SalesRecordEncoder,
            )
        else:
            salesrecords = SalesRecord.objects.filter(salepersonid = salespersonid)
            return JsonResponse(
                {"sales_records": salesrecords},
                encoder=SalesRecordEncoder,
            )
    else:
        try:
            content = json.loads(request.body)
            print("asdfa", content)
            automobiles = AutomobileVO.objects.get(vin=content["automobile"])
            content["automobile"] = automobiles

            salesperson = SalesPerson.objects.get(id=content["salesperson"])
            content["salesperson"] = salesperson

            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer

            salesrecord = SalesRecord.objects.create(**content)
            return JsonResponse(
                salesrecord,
                encoder=SalesRecordEncoder,
                safe=False
            )
    
    
        except Exception as e:
            response = JsonResponse(
                {"message": e}
            )
            response.status_code = 400
            return response




    