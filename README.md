## TO RUN THIS PROJECT:
Prerequisites: VSCODE, Docker/DockerDashboard

* Please have Docker/DockerDashboard up and running
* Please create a new directory somewhere on your computer and CLONE this project using the HTTPS Link under the    CLONE tab in gitlab. 
    - to clone, in your terminal use cmd:  git clone <https://gitlab.com/James_Egan/project-beta.git>
* CD into this project directory
* Open the project from your terminal using the cmd: code .
    - VSCODE will open with project
* back in the terminal use cmds:
    - docker volume create beta-data
    - docker-compose build
    - docker-compose up
* Check your docker to see if all the containers are up and running, and open up your browser to <http://localhost:3000/> to see the project. 

# CarCar

Team: James Egan and Jacob Kim

* James Egan - Services (Microservice)
* Jacob Kim - Sales (Microservice)
* Inventory front end - both of us


## Design
We are creating a program that will be used by a car dealership for taking inventories of cars, servicing Cars, and the selling of cars. There are the three bounded contexts in our project: Inventory, Sales, and Services
Each of those bounded contexts' will be represented by a seperate microservice. They will all be connected via the automobile value object, that links back with the automobile model in the inventory. The sales and services microservice's will both poll for data from inventory to create context. We have also included a drawing that shows these relationships down below in "CarCarDesign.png" file. 

## Inventory microservice:
Need front end that can do these things :

-Show a list of manufacturers
-Create a new manufacturer (form)

-Show a list of vehicle models
-Create a vehicle model page (form)

-Show a list of automobiles in inventory
-Create an automobile in inventory (form)


## Service microservice:
create models for:
technician
Service
automobileVO- to get info on cars

poller: need automobileVO to get data from inventory
views:

Frontend:
 - service list : list of appointments currently in the system
 - service history : list of services on a specific vin 

 - create service appointment (form)
 - create a technician (form)



## Sales microservice:
create models for:
Employee
Customer
SalesRecord
AutomobileVO


poller: need automobileVO to get data from inventory
views: 

Frontend:
- Sales Record List page that lists all sales in system
- Sales person(based on employee number) history page

- Create a sales person (employee) (form)
- Create a sales record (form)
- Create a customer (form)

