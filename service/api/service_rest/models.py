from django.db import models

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True)
    
    def __str__(self):
        return f"vin: {self.vin}"


class Technician(models.Model):
    name = models.CharField(max_length=100, unique=True)
    employee_number= models.PositiveIntegerField(unique=True)

    def __str__(self):
        return self.name

class Appointment(models.Model):
    vin = models.CharField(max_length=17)
    customer_name = models.CharField(max_length=50)
    date_time=models.DateTimeField(null=True)
    technician= models.ForeignKey(Technician,related_name="appointments",on_delete=models.PROTECT,)
    reason = models.CharField(max_length=100)
    is_finished = models.BooleanField(default=False, null=True)
    is_vip = models.BooleanField(default=False, null=True)
    
    def __str__(self):
        return f"appointment for {self.customer_name}"
